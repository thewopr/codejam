package main

import "fmt"

func main() {

	var nc int

	fmt.Scan(&nc)

	for c := 0; c < nc; c++ {
		var A, B int
		fmt.Scan(&A, &B)
		probs := make([]float64, A)
		for p := range probs {
			fmt.Scan(&probs[p])
		}
		ans := Solve(A, B, probs)
		fmt.Printf("Case #%d: %.6f\n", c+1, ans)
	}
}

func Solve(A, B int, p []float64) float64 {
	wrong := make([]float64, A+1)

	last_right := p[0]
	wrong[0] = 1 - last_right

	for i := 1; i < A; i++ {
		wrong[i] = last_right * (1 - p[i])
		last_right *= p[i]
	}

	wrong[A] = last_right
	finish := B - A + 1
	retype := B + 1

	base := float64(1 + retype)

	min := base

	for num_bs := 0; num_bs <= int(A/2); num_bs++ {
		pivot := A - num_bs
		sum := 0.0
		for idx := range wrong {
			if idx >= pivot {
				sum += float64(2*num_bs+finish) * wrong[idx]
			} else {
				sum += float64(2*num_bs+finish+retype) * wrong[idx]
			}
		}
		if sum < min {
			min = sum
		}
	}
	return min
}
