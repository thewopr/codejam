package main

import "fmt"
import "math"

func same(x string) bool {

	for i := 1; i < len(x); i++ {
		if x[i-1] != x[i] {
			return false
		}

	}
	return true
}

func rotateInt(a, digits int) int {

	return a/10 + (a%10)*int(math.Pow10(digits-1))

}

func rotate(x string) string {
	return x[len(x)-1:] + x[:len(x)-1]
}

type pair struct {
	n, m int
}

func rpairs(a, b int) int {

	m := make(map[pair]bool)

	pairs := 0
	sa := fmt.Sprintf("%d", a)
	digits := len(sa)

	for i := a; i < b; i++ {
		ti := i
		for j := 0; j < digits; j++ {
			ti = rotateInt(ti, digits)
			if ti == i || ti < i || ti > b || ti < a {
				continue
			}

			//fmt.Printf("(%d,%d)\n", i, ti)
			p := pair{i, ti}
			_, ok := m[p]
			if ok {
				continue
			}
			m[pair{i, ti}] = true
			pairs++
		}
	}
//	fmt.Printf("%v", m)
	return pairs
}

func main() {
	var nc int
	fmt.Scan(&nc)
	for i := 0; i < nc; i++ {
		var a,b int
		fmt.Scan(&a, &b)
		fmt.Printf("Case #%d: %d\n", i+1, rpairs(a, b))
	}

}
