package main

import (
	"fmt"
)

func main() {

	var nc int
	fmt.Scan(&nc)

	for c := 0; c < nc; c++ {
		var N, S, p int
		fmt.Scan(&N, &S, &p)
		t := make([]int, N)

		for idx := range t {
			fmt.Scan(&t[idx])
		}

		ans := 0
		p3 := p * 3

		for _, i := range t {
			if i >= p && (i == p3-4 || i == p3-3) && S > 0 {
				ans++
				S--
			} else if i >= p3-2 {
				ans++
			}

		}
		fmt.Printf("Case #%d: %d\n", c+1, ans)
	}

}
