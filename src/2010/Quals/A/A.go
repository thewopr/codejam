package main

import (
	"fmt"
	"bufio"
	"os"
)

var trans = map[uint8]uint8{0x67:0x76, 0x68:0x78, 0x78:0x6d, 0x79:0x61, 0x6b:0x69, 0x61:0x79, 0x6d:0x6c, 0x76:0x70, 0x6a:0x75, 0x73:0x6e, 0x75:0x6a, 0x70:0x72, 0x62:0x68, 0x63:0x65, 0x6f:0x6b, 0xa:0xa, 0x64:0x73, 0x6c:0x67, 0x65:0x6f, 0x6e:0x62, 0x20:0x20, 0x77:0x66, 0x69:0x64, 0x72:0x74, 0x74:0x77, 0x66:0x63, 0x7a: 0x71, 0x71: 0x7a}

func main() {

	in := bufio.NewReader(os.Stdin)

	ncstr, _ := in.ReadString('\n')
	var nc int
	fmt.Sscan(ncstr, &nc)

	for c := 0; c < nc; c++ {
		fmt.Printf("Case #%d: ", c+1)
		line , _ := in.ReadString('\n')

		for idx := range line {
			fmt.Printf("%c", trans[line[idx]])
		}
		fmt.Printf("\n")
	}
}
