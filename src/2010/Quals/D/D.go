package main

import (
	"container/list"
	"errors"
	"fmt"
	"math"
)

func main() {

	var ncases, H, W, D int
	fmt.Scan(&ncases)

	for c := 0; c < ncases; c++ {
		fmt.Scan(&H, &W, &D)
		room := make([]string, H)
		for l := range room {
			fmt.Scan(&room[l])
		}

		fmt.Printf("Case #%d: %d\n", c+1, do(room, D))
	}
}

func is_multiple(a, b Target) bool {
	cp := (a.x*b.y - a.y*b.x)
	dp := (a.x*b.x + a.y*b.y)
	return cp == 0 && dp > 0
}

func is_mirror(room []string, t Target) bool {
	return room[t.x][t.y] == '#'
}
func is_player(room []string, t Target) bool {
	return room[t.x][t.y] == 'X'
}

func scan_player(room []string) Target {

	for xidx, x := range room {
		for yidx, y := range x {
			if y == 'X' {
				return Target{xidx, yidx}
			}
		}

	}

	return Target{}
}

func flip(e *list.Element, dx, dy int) {
	around := e.Value.(Target)
	if dx > 0 {
		around.x++
	}
	if dy > 0 {
		around.y++
	}

	for tf := e.Next(); tf != nil; tf = tf.Next() {
		abc := tf.Value.(Target)
		if dy != 0 {
			abc.y = 2*around.y - abc.y - 1
		}
		if dx != 0 {
			abc.x = 2*around.x - abc.x - 1
		}
		tf.Value = abc
	}

}

func walk_trace(room []string, p Path) (Target, error) {
	var step *list.Element
	for step = p.Front(); step != nil && step.Next() != nil; step = step.Next() {
		st := step.Value.(Target)
		sn := step.Next().Value.(Target)
		if !is_mirror(room, sn) {
			//fmt.Println("No collision ", st, sn)
		} else {
			dx := sn.x - st.x
			dy := sn.y - st.y

			if dx != 0 && dy == 0 { //HORIZONTAL 
				//fmt.Println("Horizontal Flip at ", st, sn)
				flip(step, dx, 0)
			} else if dy != 0 && dx == 0 {
				//fmt.Println("Vertical Flip at ", st, sn)
				flip(step, 0, dy)
			} else if dx != 0 && dy != 0 { //ACTUAL CORNER CASE! X_X
				//fmt.Println("Corner Case ", st, sn)
				mx := is_mirror(room, Target{sn.x, st.y})
				my := is_mirror(room, Target{st.x, sn.y})
				//fmt.Println("mx ", mx, " my ", my)
				if !mx && !my {
					//fmt.Println("Annihalated!")
					return Target{}, errors.New("Ray was annihalated!")
				} else if mx && !my {
					//fmt.Println("Flipped one")
					flip(step, dx, 0)
				} else if !mx && my {
					//fmt.Println("Flipped one")
					flip(step, 0, dy)
				} else if mx && my {
					//fmt.Println("Flipped both!")
					flip(step, dx, dy)
				}
			}

		}
	}

	return step.Value.(Target), nil
}

func IsAlreadyFound(cur_tar, source Target, solved []Target) bool {
	for _, chk := range solved {
		if is_multiple(chk.Sub(source), cur_tar.Sub(source)) {
			//fmt.Println(chk.Sub(s), " Is a multiple of ", cur_tar.Sub(s), ism, solved)
			return true
		}
	}
	return false
}

func do(room []string, D int) int {
	s := scan_player(room)
	ts := gen_targets(s, D)
	solved := make([]Target, 0, ts.Len())

	for e := ts.Front(); e != nil; e = e.Next() {
		cur_tar := e.Value.(Target)

		if IsAlreadyFound(cur_tar, s, solved) {
			continue
		}

		p := raytrace(s, cur_tar)

		end, annihalated := walk_trace(room, p)

		if annihalated == nil && is_player(room, end) {
			solved = append(solved, cur_tar)
		}
	}

	return len(solved)
}

type Target struct {
	x, y int
}

type Path struct {
	*list.List
}

type TargetSet struct {
	*list.List
}

func gen_targets(s Target, D int) TargetSet {

	ts := TargetSet{list.New()}

	for x := 0; x <= D; x++ {
		for y := 0; y <= D; y++ {
			if dist_sq := dsquared(Target{0, 0}, Target{x, y}); dist_sq <= D*D && dist_sq != 0 {
				ts.Push(Target{s.x + x, s.y + y})
				ts.Push(Target{s.x + -x, s.y + y})
				ts.Push(Target{s.x + x, s.y + -y})
				ts.Push(Target{s.x + -x, s.y + -y})
			}
		}
	}

	return ts
}

func raytrace(s, t Target) Path {
	dx := int(math.Abs(float64(t.x - s.x)))
	dy := int(math.Abs(float64(t.y - s.y)))
	x := s.x
	y := s.y
	n := 1 + dx + dy
	var x_inc, y_inc int

	ret := Path{List: list.New()}

	if t.x > s.x {
		x_inc = 1
	} else {
		x_inc = -1
	}
	if t.y > s.y {
		y_inc = 1
	} else {
		y_inc = -1
	}
	error := dx - dy
	dx *= 2
	dy *= 2

	for ; n > 0; n-- {
		ret.PushBack(Target{x, y})

		if error > 0 {
			x += x_inc
			error -= dy
		} else if error < 0 {
			y += y_inc
			error += dx
		} else if error == 0 {
			// SLOPE IS 45 degrees
			x += x_inc
			y += y_inc
			n--
			error += dx - dy
		}
	}

	return ret
}

func dsquared(o, d Target) int {
	return (d.x-o.x)*(d.x-o.x) + (d.y-o.y)*(d.y-o.y)
}

func (p Path) String() string {
	str := "Path [ "
	for e := p.Front(); e != nil; e = e.Next() {
		str += fmt.Sprintf("%v ", e.Value)
	}
	str += "]"
	return str
}

func (ts TargetSet) String() string {
	str := "TargetSet [ "
	for e := ts.Front(); e != nil; e = e.Next() {
		str += fmt.Sprintf("%v ", e.Value)
	}
	str += "]"
	return str
}

func (ts TargetSet) Push(t Target) {
	for e := ts.Front(); e != nil; e = e.Next() {
		if e.Value == t {
			return
		}
	}
	ts.PushBack(t)
}

func (t Target) Plus(o Target) Target {
	return Target{t.x + o.x, t.y + o.y}
}

func (t Target) Sub(o Target) Target {
	return Target{t.x - o.x, t.y - o.y}
}
