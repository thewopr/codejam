package main

import "testing"
import "math"
import "fmt"

func TestSampleInput(t *testing.T) {

	type TestCase struct {
		R           []string
		D           int
		Reflections int
	}

	rooms := [][]string{
		{
			"###",
			"#X#",
			"###",
		},
		{
			"###",
			"#X#",
			"#.#",
			"###",
		},
		{
			"#######",
			"#.....#",
			"#.....#",
			"#..X..#",
			"#....##",
			"#.....#",
			"#######",
		}, {
			"######",
			"#..X.#",
			"#.#..#",
			"#...##",
			"######",
		},
	}

	tests := []TestCase{
		{R: rooms[0], D: 1, Reflections: 4},
		{R: rooms[0], D: 2, Reflections: 8},
		{R: rooms[1], D: 8, Reflections: 68},
		{R: rooms[2], D: 4, Reflections: 0},
		{R: rooms[3], D: 3, Reflections: 2},
		{R: rooms[3], D: 10, Reflections: 28},
	}

	for i, e := range tests {

		result := do(e.R, e.D)

		if result != e.Reflections {
			t.Errorf("Sample %d should have %d reflections not %d", i, e.Reflections, result)
		}

	}

}

func TestMultiple(t *testing.T) {

	type TestCase struct {
		a, b Target
		ans  bool
	}

	tests := []TestCase{
		{a: Target{1, 1}, b: Target{2, 2}, ans: true},
		{a: Target{1, 1}, b: Target{10, 10}, ans: true},
		{a: Target{2, 2}, b: Target{1, 1}, ans: true},
		{a: Target{3, 3}, b: Target{3, 3}, ans: true},
		{a: Target{2, 2}, b: Target{-2, -2}, ans: false},
	}

	for i, e := range tests {
		result := is_multiple(e.a, e.b)

		if result != e.ans {
			t.Errorf("Case %d: %v <=> %v should be %v not %v", i, e.a, e.b, e.ans, result)
		}

	}
}

func TestGenTargets(t *testing.T) {
	tees := gen_targets(Target{0, 0}, 2)

	if tees.Len() != 12 {
		t.Errorf("There should be 12 targets not %d", tees.Len())
	}
}

func ExampleGenTargets() {

	for D := 1; D <= 50; D++ {
		tees := gen_targets(Target{0, 0}, D)
		fmt.Printf("D == %d len(%d) %f", D, tees.Len(), float64(D)*float64(D)*math.Pi)
	}
}
