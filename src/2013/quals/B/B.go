package main

import (
	"fmt"
)

func PrintSolution(cs int, p bool) {
	if p {
		fmt.Printf("Case #%d: YES\n", cs+1)
	} else {
		fmt.Printf("Case #%d: NO\n", cs+1)
	}
}

type Lawn [][]int

func (l Lawn) Border(r, c int) bool {
	switch {
	case r == 0:
		return true
	case c == 0:
		return true
	case r == l.Rows()-1:
		return true
	case c == l.Cols()-1:
		return true
	default:
		return false
	}

	return false

}
func (l Lawn) Rows() int {
	return len(l)
}
func (l Lawn) Cols() int {
	return len(l[0])
}

func (l Lawn) WayOut(r, c int) bool {
	hor := true
	for ir := 0; ir < l.Rows(); ir++ {
		if (ir != r) && l[ir][c] > l[r][c] {
			hor = false
		}
	}
	vert := true
	for ic := 0; ic < l.Cols(); ic++ {
		if (ic != c) && l[r][ic] > l[r][c] {
			vert = false
		}
	}
	return hor || vert
}

func main() {

	var nc int
	fmt.Scan(&nc)

	for cs := 0; cs < nc; cs++ { 
		var M, N int

		fmt.Scan(&M, &N)

		var lawn Lawn
		lawn = make([][]int, M)

		for r := range lawn {
			lawn[r] = make([]int, N)
		}

		for r := range lawn {
			for c := range lawn[r] {
				fmt.Scan(&lawn[r][c])
			}
		}

		if M == 1 || N == 1 {
			PrintSolution(cs, true)
			continue
		}


		done := false

		for r := range lawn {
			for c := range lawn[r] {
				if !lawn.WayOut(r, c) {
					PrintSolution(cs, false)
					done = true
					break
				}
			}
			if done == true {
				break
			}
		}

		if done != true {
			PrintSolution(cs, true)
		}
	}
}
