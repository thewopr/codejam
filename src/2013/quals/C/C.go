package main

import "fmt"
import "strings"
import "sort"
import "math/big"

func main() {

	GenToX(50)
	sort.Strings(FAS)
	hfas = make([]*big.Int, len(FAS))
	two := big.NewInt(2)
	for i, x := range FAS {
		hfas[i] = big.NewInt(0)
		hfas[i].SetString(strings.Trim(x, " "), 10)
		hfas[i].Exp(hfas[i], two, nil)
	}

	var T int
	var A, B big.Int

	fmt.Scan(&T)

	for t := 0; t < T; t++ {

		fmt.Scan(&A, &B)
		low, high := -1, -1

		for idx, ref := range hfas {
			if low == -1 && ref.Cmp(&A) >= 0 {
				low = idx
			}

			if low != -1 && ref.Cmp(&B) > 0 {
				high = idx
				break
			}
		}

		fmt.Printf("Case #%d: %d\n", t+1, high-low)
	}

}

var FAS []string
var hfas []*big.Int

func IsP(s int64) bool {
	return IsPS(fmt.Sprintf("%d", s))
}

func IsPS(s string) bool {
	return reverse(s) == s
}

func reverse(s string) string {
	orig := s
	len := len(s)
	n := make([]byte, len)
	for i := 0; i < len; i++ {
		n[len-i-1] = orig[i]
	}
	return string(n)
}

func GenToX(X int) (ret []string) {
	FAS = append(FAS, fmt.Sprintf("%*s", X+1, "1"))
	FAS = append(FAS, fmt.Sprintf("%*s", X+1, "2"))
	FAS = append(FAS, fmt.Sprintf("%*s", X+1, "3"))
	rs := make([]string, X)
	for z := range rs {
		rs[z] = "0"
	}

	for n1 := 1; n1 <= 4; n1++ {
		combinations(X/2, n1, func(inds []int) {
			last := inds[len(inds)-1]
			for _, i := range inds {
				rs[i] = "1"
			}
			Srs := strings.Join(rs[:last+1], "")
			FAS = append(FAS, fmt.Sprintf("%*s", X+1, fmt.Sprintf("%s%s%s", reverse(Srs), "", Srs)))
			FAS = append(FAS, fmt.Sprintf("%*s", X+1, fmt.Sprintf("%s%s%s", reverse(Srs), "0", Srs)))
			FAS = append(FAS, fmt.Sprintf("%*s", X+1, fmt.Sprintf("%s%s%s", reverse(Srs), "1", Srs)))
			if (n1*2 + 2*2) < 10 {
				FAS = append(FAS, fmt.Sprintf("%*s", X+1, fmt.Sprintf("%s%s%s", reverse(Srs), "2", Srs)))
			}
			for _, i := range inds {
				rs[i] = "0"
			}
		})

	}

	for l := 1; l <= X/2; l++ {
		rs[l-1] = "2"
		Srs := strings.Join(rs[:l], "")
		FAS = append(FAS, fmt.Sprintf("%*s", X+1, fmt.Sprintf("%s%s%s", reverse(Srs), "", Srs)))
		FAS = append(FAS, fmt.Sprintf("%*s", X+1, fmt.Sprintf("%s%s%s", reverse(Srs), "0", Srs)))
		FAS = append(FAS, fmt.Sprintf("%*s", X+1, fmt.Sprintf("%s%s%s", reverse(Srs), "1", Srs)))
		rs[l-1] = "0"
	}

	return ret
}

func combinations(size, choice int, do func([]int)) {

	n := size

	if choice > n || choice == 0 {
		return
	}

	indices := make([]int, choice)
	for i := range indices {
		indices[i] = i
	}
	do(indices)

	for {
		i := choice - 1
		for ; i >= 0 && indices[i] == i+n-choice; i -= 1 {
		}

		if i < 0 {
			return
		}

		indices[i] += 1
		for j := i + 1; j < choice; j += 1 {
			indices[j] = indices[j-1] + 1
		}

		do(indices)
	}

}
