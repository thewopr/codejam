package main

import (
	"fmt"
	"strings"
)

func main() {

	var nc int
	fmt.Scan(&nc)

	for cs := 0; cs < nc; cs++ {
		b := make(Board, 4)
		for r := 0; r < 4; r++ {
			fmt.Scan(&b[r])
		}

		var win string

		runs := make([][]Cell, 0, 2 + 2*b.Size())

		runs = append(runs, b.GetDiagonals()...)
		runs = append(runs, b.GetRows()...)
		runs = append(runs, b.GetColumns()...)

		for i := 0; i < len(runs) && win == ""; i++ {
			win = b.CheckRun(runs[i])
		}

		switch {
		case win != "":
			fmt.Printf("Case #%d: %s won\n", cs+1, string(win))
		case b.Full() == true:
			fmt.Printf("Case #%d: Draw\n", cs+1)
		default:
			fmt.Printf("Case #%d: Game has not completed\n", cs+1)
		}
	}
}

type Cell [2]int
type Board []string

func (b Board) Size() int { return len(b) }
func (b Board) Full() bool {

	for _, r := range b {
		if strings.Contains(r, ".") {
			return false
		}
	}

	return true
}

func (b Board) String() string {
	return fmt.Sprintf("%s\n%s\n%s\n%s", b[0], b[1], b[2], b[3])
}

func (b Board) GetDiagonals() (dees [][]Cell) {

	dees = make([][]Cell, 2)
	dees[0] = make([]Cell, b.Size())
	dees[1] = make([]Cell, b.Size())

	for r := 0; r < b.Size(); r++ {
		dees[0][r] = Cell{r, r}
		dees[1][r] = Cell{r, b.Size() - r - 1}
	}

	return dees
}

func (b Board) GetRows() (rows [][]Cell) {
	rows = make([][]Cell, b.Size())
	for r := 0; r < b.Size(); r++ {
		rows[r] = make([]Cell, b.Size())
		for c := 0; c < b.Size(); c++ {
			rows[r][c] = Cell{r, c}
		}
	}

	return rows
}
func (b Board) GetColumns() (cols [][]Cell) {
	cols = make([][]Cell, b.Size())
	for c := 0; c < b.Size(); c++ {
		cols[c] = make([]Cell, b.Size())
		for r := 0; r < b.Size(); r++ {
			cols[c][r] = Cell{r, c}
		}
	}
	return cols
}

func (b Board) CheckRun(run []Cell) string {
	var X, O int

	for _, cell := range run {

		switch b[cell[0]][cell[1]] {

		case 'X':
			X++
		case 'O':
			O++
		case 'T':
			X++
			O++
		}
	}

	switch {
	case X == b.Size():
		return "X"
	case O == b.Size():
		return "O"
	}

	return ""
}
