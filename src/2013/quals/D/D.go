package main

import "fmt"

func main() {
	var nc int

	fmt.Scan(&nc)

	for c := 0; c < nc; c++ {

		chests, keys := ParseInput()

		var ans []int

		if EnoughKeys(chests, keys) {
			ans = Solve(chests, keys)
		}

		if ans != nil {
			fmt.Printf("Case #%d:", c+1)
			for a := len(ans) - 1; a >= 0; a-- {
				fmt.Printf(" %d", ans[a])
			}
			fmt.Println()
		} else {
			fmt.Printf("Case #%d: IMPOSSIBLE\n", c+1)
		}

	}

}

func EnoughKeys(cs []Chest, keys []int) bool {

	for idx := range cs {
		cs[idx].Unlock(keys, true)
		defer cs[idx].Relock(keys, true)
	}

	for _, k := range keys {
		if k < 0 {
			return false
		}
	}
	return true
}

func Solve(chests []Chest, keys []int) (ret []int) {
	for i := 0; i < len(chests) && ret == nil; i++ {
		ret = VisitChest(i, chests, keys, 1)
	}
	return ret
}

func Solvable(cs []Chest, keys []int) bool {
	for {
		open := 0
		changed := false
		for idx := range cs {
			if !cs[idx].Open && cs[idx].Openable(keys) {
				cs[idx].Unlock(keys, false)
				defer cs[idx].Relock(keys, false)
				changed = true
			}
			if cs[idx].Open {
				open++
			}
		}
		if !changed {
			return false
		}
		if open == len(cs) {
			return true
		}
	}
	return true
}

func VisitChest(c int, chests []Chest, keys []int, depth int) (ret []int) {
	if !chests[c].Open && chests[c].Openable(keys) {
		chests[c].Unlock(keys, true)

		if depth == len(chests) {
			return []int{c + 1}
		}

		if Solvable(chests, keys) {
			for i := 0; i < len(chests) && ret == nil; i++ {
				ret = VisitChest(i, chests, keys, depth+1)
				if ret != nil {
					return append(ret, c+1)
				}
			}
		}

		chests[c].Relock(keys, true)
	}
	return nil
}

type Chest struct {
	Open bool
	Lock int
	Keys []int
}

func (c Chest) String() string {
	if c.Open {
		return fmt.Sprintf("(%d Empty)", c.Lock)
	}
	return fmt.Sprintf("(%d %v)", c.Lock, c.Keys)
}

func (c *Chest) Openable(keys []int) bool {
	return keys[c.Lock] > 0
}

func (c *Chest) Unlock(keys []int, consume bool) {
	c.Open = true
	if consume {
		keys[c.Lock]--
	}

	for _, k := range c.Keys {
		keys[k]++
	}
}

func (c *Chest) Relock(keys []int, consume bool) {
	c.Open = false
	if consume {
		keys[c.Lock]++
	}

	for _, k := range c.Keys {
		keys[k]--
	}
}

func ParseInput() (chests []Chest, keys []int) {
	var K, N int
	fmt.Scan(&K, &N)

	ks := make([]int, 201)
	cs := make([]Chest, N)

	for k := 0; k < K; k++ {
		var key int
		fmt.Scan(&key)
		ks[key]++
	}

	for c := 0; c < N; c++ {
		var nkeys int
		fmt.Scan(&cs[c].Lock)
		fmt.Scan(&nkeys)
		cs[c].Keys = make([]int, nkeys)
		for i := 0; i < nkeys; i++ {
			fmt.Scan(&cs[c].Keys[i])
		}
	}

	return cs, ks
}
