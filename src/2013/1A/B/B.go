package main

import (
	"fmt"
	"math"
)

func main() {

	var N, T int
	var E, R int64

	fmt.Scan(&T)

	for t := 0; t < T; t++ {

		fmt.Scan(&E, &R, &N)

		V := make([]int64, N)

		for vi := range V {
			fmt.Scan(&V[vi])
		}

		fmt.Printf("Case #%d: %d\n", t+1, Solve(E, R, V))
	}

}

func Solve(max_energy, rest_energy int64, V []int64) int64 {

	total := int64(0)

	if rest_energy > max_energy {
		rest_energy = max_energy
	}

	current_energy := max_energy

	next_max := 0
	la := int(math.Ceil(float64(max_energy / rest_energy)))

	for idx := range V {

		to_spend := current_energy

		if next_max == idx {
			max++
		}

		for li := next_max; li <= idx+la && li < len(V); li++ {
			if V[li] > V[idx] {
				to_spend = (current_energy + rest_energy*int64(li-idx)) - max_energy
				next_max = li
				if to_spend < 0 {
					to_spend = 0
				}
				break
			}
		}

		current_energy -= to_spend
		current_energy += rest_energy
		total += V[idx] * to_spend

	}

	return total
}
