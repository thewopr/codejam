package main

import (
	"fmt"
	"math/big"
)

var I big.Int

func SqrtBig(n *big.Int) (x *big.Int) {
	switch n.Sign() {
	case -1:
		panic(-1)
	case 0:
		return big.NewInt(0)
	}

	var px, nx big.Int
	x = big.NewInt(0)
	x.SetBit(x, n.BitLen()/2+1, 1)
	for {
		nx.Rsh(nx.Add(x, nx.Div(n, x)), 1)
		if nx.Cmp(x) == 0 || nx.Cmp(&px) == 0 {
			break
		}
		px.Set(x)
		x.Set(&nx)
	}
	return
}

func RingsToPaint(n *big.Int) *big.Int {
	return I.Div(I.Add(I.Mul(n, n), n), big.NewInt(2))
}

func NRings(X *big.Int) *big.Int {

	// X = (x^2 + x) / 2

	one := big.NewInt(1)
	two := big.NewInt(2)
	eight := big.NewInt(8)

	disc := SqrtBig(I.Add(one, I.Mul(eight, X)))
	root := I.Div(I.Sub(disc, one), two)

	return root
}

func Solve(start, paint *big.Int) *big.Int {

	start.Sub(start, big.NewInt(1))

	paint.Add(paint, RingsToPaint(start))

	T := NRings(paint)

	T.Sub(T, start)

	return I.Div(T, big.NewInt(2))
}

func main() {

	var nc int
	fmt.Scan(&nc)

	for cs := 0; cs < nc; cs++ {
		var R, T int64
		fmt.Scan(&R, &T)
		fmt.Printf("Case #%d: %d\n", cs+1, Solve(big.NewInt(R), big.NewInt(T)))
	}
}
